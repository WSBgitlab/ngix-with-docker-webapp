# Configuração de Ambiente de Desenvolvimento 
Configuração iniciais do ambiente de Desenvolvimento da Ventura Working 

## Getting Started
Primeiramente temos que ter no nosso host o docker instalado no meu caso instalei em um Debian 9 64x
aqui tem alguns artigos para a instalação.

```
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-9
```

## Prerequisites
Host com pelo menos 8GB de RAM e no minimo 50GB de armazenamento, para começo da aplicação a partir da escalação de serviços e de processamento, logo terá a necessidade de aumentar a capacidade de ambos.

Para seguir as boas práticas de armazenamento bom configurar (Logical Volume Manager) LVM para a manipulação de armazenamento mais prático.

## Instaling

```
    docker pull postgres / docker pull dpage/pgadmin4
```
*Instalações das ferramentas de desenvolvimento.*
    
Docker configurações Banco de Dados

<h2> Docker - Postgresql </h2>

```
docker run 
--name postgres 
--network=postgres-network 
-e "POSTGRES_USER=your_user" 
-e "POSTGRES_PASSWORD=your_password" 
-e "POSTGRES_DB=bd_hack" 
-p 5432:5432
-v /home/wellington/Desenvolvimento/PostgreSQL:/var/lib/postgresql/data 
-d postgres
```


Onde : <br>

``` 
    -e "POSTGRES_USER=your_user" \ Será seu email de login para o sistema web pgAdmin.
```    

onde : <br>

```
    -e "POSTGRES_PASSWORD=your_password" \ Será sua senha de login para o sistema web pgAdmin.
```

onde : <br>

```
    -e "POSTGRES_DB=bd_hack" \ Será sua senha de login para o sistema web pgAdmin.
```

<h2> Docker - PGAdmin </h2>


```
    docker run 
    --name pgAdmin 
    --network=postgres-network  
    -p 15432:80 
    -e "PGADMIN_DEFAULT_EMAIL=your_email"  
    -e "PGADMIN_DEFAULT_PASSWORD=your_password" 
    -d dpage/pgadmin4 
```

<h2> Descrição dos parâmetros. </h2>

```
    --name      : Nome da imagem.
    
    --network   : Rede entre container, é necessário a criação desse driver com o comando : 
        docker network create --driver bridge nome_da_rede
    
    -e          : Variaveis de Ambiente do container
    -p          : Port (internal) : (external)
    -v          : Docker irá gerar documentos no disco interno com "espelho" digamos assim para o container
    -d          : Executar em background
```



<h2>Jenkis Container</h2>

Container Jekins 

## Instaling
```
    https://github.com/jenkinsci/docker/blob/master/README.md
```

-> Diretório de instalação no github.

docker run -p porta_desocupadas:8080 -p 50000:50000 jenkins/jenkins:lts

coloquei porta_desocupadas para relembrar quando executarmos a solicitação da instalação, nós lembrarmos quais são as portas em uso para evitar conflitos de portas!

Nesse momento a instalação será executada importante dizer, que temos que armazenar o código de instalação digamo que nada mais é que um "password" para a configuração do jekins

semelhante a esse : 

*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

código_md5 aqui será sua senha para o primeiro login na interface web

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
	

<h2>Configurações Ngix (Web Server)</h2>
## Getting Started
Instalação ambiente web para rodar interface web com reajs e nodejs.

## Prerequisites
Docker instalado

## Instaling
```
    docker pull nginx

    https://hub.docker.com/_/nginx
```

Para a utilização do servidor web com docker temos que ter em mente que a porta será espelhada, por exemplo
porta 80 do host 192.168.1.20, quando fazermos uma requisição GET será redirecionada para a 80 do docker nginx.

<h3><i>Proxy Reverso</i></h3>

Proxy reverso consiste em diminuir consumos na memória RAM e deixar a aplicação mais rápida, em um exemplo no link utilizou-se desse recurso
onde o Apache2 ficou com a parte do processamento (no-static-files) e o nginx com arquivos estáticos como Js , css e imagens.
Com isso a aplicação ficara mais rápida por questões que cada servidor HTTP tem suas depedencia e papel na sua aplicação isso funciona muito bem.
Criamos também um proxy reverso no servidor nginx para que o servidor nodejs se cominique com o server http.

<h3><i>Configurando Log Nginx</i></h3>

Como sabemos preservar logs é uma das coisas mais importântes em uma aplicação, principalmente web que tem uma porta aberta para a internet. Contudo o Nginx com sua 
configuração padrão, não vem configurado a porta logica da requisição do client side. Geralmente os provedores usam o padrão de IP dinâmico que consiste em usar o mesmo 
IP para diversas empresas mudando somente sua porta lógica de origem. 
Se estivermos falando de hackers kids (hackers iniciantes), alguns poderá não estar ultilizando uma VPN para se mascarar e provávelmente não terá um ip fixo atrelado a ele.
Portanto com a porta lógica podemos saber com detalhes informações cruciais sobre o hacker.

Por exemplo :
```
    empresa 1 ---- IP : 768.234.212.451 --- Porta : 1234
    empresa 2 ---- IP : 768.234.212.451 --- Porta : 5678 -- Attack
    empresa 3 ---- IP : 768.234.212.451 --- Porta : 9101
```

<b>configuração Nginx path : /etc/nginx/nginx.config</b>

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 768;
    # multi_accept on;
}

http {
    ##
    # Basic Settings
    ##


    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    # server_tokens off;

    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # CONFIGURAÇÃO DO LOG NGINX 
    # VARIAVEIS http://nginx.org/en/docs/varindex.html

    log_format  main  '$remote_addr - Logic Port $remote_port - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';



    ##
    # SSL Settings
    ##

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    ##
    # Logging Settings
    ##
    
    # main na frente do comando chamando configuração do log!
    # o mesmo serve para o error.log configure do jeito que achar melhor.
    access_log /var/log/nginx/access.log main;
    error_log /var/log/nginx/error.log;

    ##
    # Gzip Settings
    ##

    gzip on;

    # gzip_vary on;
    # gzip_proxied any;
    # gzip_comp_level 6;
    # gzip_buffers 16 8k;
    # gzip_http_version 1.1;
    # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    ##
    # Virtual Host Configs
    ##

    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
```

<h2>Configurando Nodejs com Nginx</h2>

arquivo de configuração : /etc/nginx/sites-available/default

```
server {
        #escutando porta 80
        listen 80;
        
        #path root da aplicação
        root /var/www/html/frontend/;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        
        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                # try_files $uri $uri/ =404;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
                # Proxy pass server para quando bater no root path ir para o proxy desejavel 
                # No nosso caso nossa aplicação Reacjs (frontend)
                proxy_pass http://localhost:5555;   
        }

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;
        #
        #       # With php7.0-cgi alone:
        #       fastcgi_pass 127.0.0.1:9000;
        #       # With php7.0-fpm:
        #       fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #       deny all;
        #}
}
```


<h3>Autenticações<h3>
<br>
```
    [+] ----------------------------------------------------------------- [+]

    ---------------------------- Controle de Portas -------------------------

    [+] ----------------------------------------------------------------- [+]

           ____ports____             |          ____Services____
         192.168.1.25:8080                      Ngix    (WebServer)
         192.168.1.25:15432                     pgAdmin (SGBD)
         192.168.1.25:4000                      Jekins  (CI/CD)

    - VM -
    ssh ventura@192.168.1.25
    password -> #ventura#2k19$

    - 192.168.1.25:15432 -
    User -> wellington@venturaerm.com
    Pass -> #postgresql#2k19$

    - 192.168.1.25:4000 -
    User -> wellington
    Pass -> #jenkins#2k19$

```