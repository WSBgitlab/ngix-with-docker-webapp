const express = require('express');
const body = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');

const routersGets = require('./src/routes/routersGets');
const routersPosts = require('./src/routes/routersPosts');

const Mongoose = require('./src/database/model/MongoDB/config');

//Configure variables ambient
dotenv.config();

//App running express
const app = express();

// Connect with database
Mongoose.connect();

//We did it to working for with json on requisitions
app.use(express.json());

//Accept visualization params in requisition body
app.use(body.urlencoded({ extended: true }));

//cors
app.use(cors())

// Routers GET
app.use(routersGets);
// Routers POST
app.use(routersPosts);
//App Listening on port 5555


app.listen(5555); 