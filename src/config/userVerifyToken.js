const jwt = require('jsonwebtoken');

/**
 * Function working in authenticate users with token only jwt
 */

module.exports = function(req,res,next){
    //getting header token
    const token = req.header('token-user');

    //Requisition is null token!
    if(!token) return res.status(401).send('Access Denied!');
    
    try{
        //Token Verify jwt token in header for compare with token secret variable.
        const tokenVerify = jwt.verify(token, process.env.TOKEN_SECRET);
        //before include in header requisition user with value token.
        req.user = tokenVerify;

        next();
        //No freeze middlewares
    }catch(error) {
        return res.status(400).send('Invalid Token');
    }
}    