//Lib for encryption
const bcrypt = require('bcryptjs');

//JWT
const jwt = require('jsonwebtoken');

//Model Users
const userSchema = require('./../database/model/MongoDB/User');

//Utils validations
const Validate = require('./../utils/validations');

class Auth {
    async register(req,res,next){
        const {
            user : name,
            email,
            password
        } = req.body;
        
        /**
            Importing function validateUserRegister the validations Util
            Working validate of Register the users.
         */
        
         // If that there error in validateUserRegister.
        const { error } = Validate.validateUserRegister(req.body);
        
        //Returning one error with validation in Joi conditions.
        if (error) return res.status(400).send(error.details[0].message);
        //Checking if user exist in database
        const emailExist = await userSchema.findOne({email});

        //Returning that there is one user exist with this email;
        if (emailExist) return res.status(400).json({ emailExist : "Email existente" });

        //Calculate Hash passed for password, We can salt's in calculate hash;
        const salt = await bcrypt.genSalt(10);
        //Create Hash with salt in calculate execute;
        const hashPwd = await bcrypt.hash(password, salt);

        // Save in database new register of users.
        try { 
            //Doing insert in database MongoDB
            userSchema.create({name,email,password : hashPwd}, (erro, result) => {
                if (erro) res.status(400).json({ erro : "Error ao inserir o usuario!" });
                
                return res.status(200).json(result);
            });
        }catch(err){ 
            return res.status(400).json({ erro : err});
        }        
    }


    async login(req,res,next){
        const { email , password } = req.body;
        //Validate Email and password
        const { error } = Validate.validateUserLogin(req.body);

        if (error) return res.status(400).send(error.details[0].message);
        
        //Validate email
        const user = await userSchema.findOne({email}); 
        

        if(!user){
            return res.send({ errorEmail : "Email not found!" });
        }else{
            /**
             * validatePass
             *  irá comparar o password da requisição com o user.password do email.
             */
            const validatePass = await bcrypt.compare(password , user.password);

            if(!validatePass) return res.status(400).send('Invalid password not found!');
        }
        

        //JWT Token
        const token = await jwt.sign({ _id : user._id }, process.env.TOKEN_SECRET,{ expiresIn: '30m' });
        //Insert jwt in header httpX
        res.header('token-user', token);

        const decodeToken = await jwt.decode(token, { complete : true });
        const idUser = await decodeToken.payload._id;

        //If exist _id in database
        const tokenIdValidate = await userSchema.findOne({ _id : idUser});

        if(!tokenIdValidate._id) return res.send().json({ idToken : "Error ID Token not exists in database!" });

        return res.status(200)
            .json({ 
                login : true,
                token,
                idUser,
            }); 
    } 
}
module.exports = new Auth;