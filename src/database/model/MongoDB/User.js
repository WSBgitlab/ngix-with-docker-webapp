const mongoose = require('mongoose');

const users = new mongoose.Schema({
    name  :{
        type : String,
        required : true,
        min : 6
    },
    email : {
        type : String,
        required : true,
        min : 8
    },
    password : {
        type : String,
        required : true,
        min : 8
    }
},{
    timestamps : true
});


module.exports = mongoose.model('Users', users);