const Mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

class MongodDB {
    async connect(){
        // console.log('MongoDB classs',process.env.DB_CONNECT);
        const connect = await Mongoose.connect('mongodb+srv://Ventura:C6O3ojQeD2Y3j8rx@clusterprovi-sambu.mongodb.net/test?retryWrites=true&w=majority', {
            useNewUrlParser : true
        });
        
        const connected = connect.connection;

        return connected._readyState; 
    }
}

module.exports = new MongodDB;