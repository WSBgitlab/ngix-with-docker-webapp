const express = require('express');

const tokenValidate = require('./../config/userVerifyToken');

const routersGet = express.Router();


routersGet.get('/api/v0/test', tokenValidate ,(req,res,next) => {
    res.json({
        post : {
            name : "Secret name",
            password : "Secret password",
        }
    });
});

module.exports = routersGet;