const express = require('express');

const controllerAuth = require('./../controller/Auth');

const routersPost = express.Router();

routersPost.post('/api/v0/register', controllerAuth.register);
routersPost.post('/api/v0/login', controllerAuth.login);

module.exports = routersPost;