/**
 * Object schema description language and validator for JavaScript objects.
 */
const Joi = require('@hapi/joi');

//Function Validate Joi Register Users

/**
 *  Essa classe será ultlizada para validações dos campos
 *  Seguindo um padrão de inputs
 * 
 *  Utilizamos a lib Joi ('@hapi/joi')
 * 
 */
class Validate { 

    /**
     *  Função irá receber requisições via params
     *  a partir desses campos fazemos o Schema desejado
     *  com validações dos campos.
     * 
     *  req : format_string
     *  
     */
    validateUserRegister = (req) => {
        const schemaUser = {
            user : Joi.string().min(6).required(),
            email : Joi.string().min(6).required(),
            password : Joi.string().min(10).required()   
        };
        
        //Return a verificação se a requisição está de acordo com o schema.
        return Joi.validate(req , schemaUser);
    }
    
    /**
     *  Função irá receber requisições via params
     *  a partir desses campos fazemos o Schema desejado    
     *  com validações dos campos.
     */
    validateUserLogin = (req) => {
        const schemaLogin = {
            email : Joi.string().min(8).required(),
            password : Joi.string().min(10).required(),
        }

        //Return a verificação se a requisição está de acordo com o schema.
        return Joi.validate(req , schemaLogin);
    }   
}

module.exports = new Validate;